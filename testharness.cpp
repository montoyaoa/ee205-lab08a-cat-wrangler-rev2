///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file testharness.cpp
/// @version 1.0
///
/// Used to test the Node and DoubleLinkedList classes
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   06_APR_2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <cstring>
#include "node.hpp"
#include "list.hpp"

using namespace std;
void listStats(DoubleLinkedList);
string addrToName(Node*);


Node node1, node2, node3, node4, node5, node6, node7, node8, node9;
DoubleLinkedList list;

int main() {
   cout << "good" << endl;
   cout << list.size() << endl;
   listStats(list);

   cout << "add node 1: " << &node1 << endl;
   list.push_front(&node1);            //n1
   cout << "add node 2: " << &node2 << endl;
   list.push_front(&node2);            //n2 n1
   cout << "add node 3: " << &node3 << endl;
   list.push_front(&node3);            //n3 n2 n1
   listStats(list);
   
   cout << "remove node 3" << endl;
   list.pop_front();                   //n2 n1
   listStats(list);
   if(list.get_first() == &node2) {
      cout << "The first node is node2" << endl;
   }
   if(list.get_next(list.get_first()) == &node1) {
      cout << "The next node is node1" << endl;
      cout << endl;
   }
   cout << "add node 3 to end" << endl;
   list.push_back(&node3);             //n2 n1 n3
   listStats(list);
   cout << "remove node3 from end" << endl;
   list.pop_back();                    //n2 n1
   listStats(list);
   if(list.get_last() == &node1) {
      cout << "The last node is node1" << endl;
   }
   if(list.get_prev(list.get_last()) == &node2) {
      cout << "The previous node is node2" << endl;
      cout << endl;
   }
   cout << "add node 3 to front of list" << endl;
   list.push_front(&node3);            //n3 n2 n1
   listStats(list);
   cout << "add node 4 after node 2: " << &node4 << endl;
   list.insert_after(&node2, &node4);  //n3 n2 n4 n1
   listStats(list);
   cout << "remove node 3 from front of list" << endl;
   list.pop_front();                   //n2 n4 n1
   listStats(list);
   cout << "insert node 3 after node 1" << endl;
   list.insert_after(&node1, &node3);  //n2 n4 n1 n3
   listStats(list);
   cout << "remove node 3 from back" << endl;
   list.pop_back();                    //n2 n4 n1
   listStats(list);
   cout << "remove node 1 from back" << endl;
   list.pop_back();                    //n2 n4
   listStats(list);
   cout << "remove node 4 from back" << endl;
   list.pop_back();                    //n2
   listStats(list);
   cout << "remove node 2 from back" << endl;
   list.pop_back();                    //empty
   listStats(list);
   cout << "swap an empty list" << endl;
   list.swap(list.get_first(), list.get_last());
   listStats(list);
   cout << "add node 1 to back" << endl;
   list.push_back(&node1);             //n1
   listStats(list);
   cout << "swap a list of one" << endl;
   list.swap(list.get_first(), list.get_last());
   listStats(list);
   cout << "add node 2 to back" << endl;
   list.push_back(&node2);             //n1 n2
   listStats(list);
   cout << "swap node 1 (head) and node 2 (tail)" << endl;
   list.swap(list.get_first(), list.get_last());   //n2 n1
   listStats(list);
   cout << "swap node 1 (head) and node 2 (tail) again" << endl;
   list.swap(list.get_first(), list.get_last());   //n1 n2
   listStats(list);
   cout << "add node 3 to back" << endl;
   list.push_back(&node3);             //n1 n2 n3
   listStats(list);
   cout << "swap node 1 (head) and node 3 (tail)" << endl;
   list.swap(list.get_first(), list.get_last());   //n3 n2 n1
   listStats(list);
   cout << "swap node 1 (head) and node 3 (tail) again" << endl;
   list.swap(list.get_first(), list.get_last());   //n1 n2 n3
   listStats(list);
   cout << "add node 4 to back" << endl;
   list.push_back(&node4);             //n1 n2 n3 n4
   listStats(list);
   cout << "swap node 2 and node 3" << endl;
   list.swap(&node2, &node3);   //n1 n3 n2 n4
   listStats(list);
   cout << "swap node 2 and node 3 again" << endl;
   list.swap(&node2, &node3);   //n1 n2 n3 n4
   listStats(list);
   cout << "add node 5 to back: " << &node5 << endl;
   list.push_back(&node5);             //n1 n2 n3 n4 n5
   listStats(list);
   cout << "swap node 2 and node 4" << endl;
   list.swap(&node2, &node4);   //n1 n4 n3 n2 n5
   listStats(list);
   cout << "swap node 2 and node 4 again" << endl;
   list.swap(&node2, &node4);   //n1 n2 n3 n4 n5
   listStats(list);
   cout << "swap node 1 (head) and node 3" << endl;
   list.swap(&node1, &node3);   //n3 n2 n1 n4 n5
   listStats(list);
   cout << "swap node 1 (head) and node 3 again" << endl;
   list.swap(&node1, &node3);   //n1 n2 n3 n4 n5
   listStats(list);
   cout << "swap node 5 (tail) and node 3" << endl;
   list.swap(&node5, &node3);   //n1 n2 n5 n4 n3
   listStats(list);
   cout << "swap node 5 (tail) and node 3 again" << endl;
   list.swap(&node5, &node3);   //n1 n2 n3 n4 n5
   listStats(list);
   cout << "swap node 1 and node 5" << endl;
   list.swap(&node1, &node5);   //n5 n2 n3 n4 n1
   listStats(list);
   cout << "swap node 4 and node 2" << endl;
   list.swap(&node4, &node2);   //n5 n4 n3 n2 n1
   listStats(list);              //should be sorted now
   cout << "swap node 5 and node 1" << endl;
   list.swap(&node5, &node1);   //n1 n4 n3 n2 n5
   listStats(list);             
   cout << "swap node 4 and node 2" << endl;
   list.swap(&node4, &node2);   //n1 n2 n3 n4 n5
   listStats(list);             
   cout << "insertion sort list" << endl;
   list.insertionSort();         //n5 n4 n3 n2 n1
   listStats(list);

   cout << "remove all nodes" << endl;
   list.pop_front();             //n4 n3 n2 n1
   list.pop_front();             //n3 n2 n1
   list.pop_front();             //n2 n1
   list.pop_front();             //n1
   list.pop_front();             //empty
   cout << "add all 9 nodes" << endl;
   list.push_front(&node5);
   list.push_back(&node8);
   list.push_front(&node2);
   list.push_back(&node3);
   list.push_front(&node1);
   list.push_back(&node4);
   list.push_front(&node7);
   list.push_back(&node6);
   list.push_front(&node9);
   listStats(list);
   list.insertionSort();
   listStats(list);

}

void listStats(DoubleLinkedList inputList) {
   int count = 0;
   Node* currentNode = inputList.get_first();
   cout << addrToName(currentNode);
   while((currentNode != nullptr) && (count < 10)) {
      cout << "->" << addrToName(inputList.get_next(currentNode));
      currentNode = inputList.get_next(currentNode);
      count++;
   }
   cout << endl;
   count = 0;
   currentNode = inputList.get_last();
   cout << addrToName(currentNode);
   while((currentNode != nullptr) && (count < 10)) {
      cout << "->" << addrToName(inputList.get_prev(currentNode));
      currentNode = inputList.get_prev(currentNode);
      count++;
   }
   cout << endl;
   cout << "Is list empty: " << boolalpha << inputList.empty() << endl;
   cout << "Size of list: " << inputList.size() << endl;
   cout << "Head pointer: " << addrToName(inputList.get_first()) << endl;
   cout << "Tail pointer: " << addrToName(inputList.get_last()) << endl;
   cout << "Is list sorted: " << boolalpha << inputList.isSorted() << endl;

   cout << endl;
}

string addrToName(Node* inputNode) {
   if(inputNode == &node1)
      return "node 1";
   else if (inputNode == &node2)
      return "node 2";
   else if (inputNode == &node3)
      return "node 3";
   else if (inputNode == &node4)
      return "node 4";
   else if (inputNode == &node5)
      return "node 5";
   else if (inputNode == &node6)
      return "node 6";
   else if (inputNode == &node7)
      return "node 7";
   else if (inputNode == &node8)
      return "node 8";
   else if (inputNode == &node9)
      return "node 9";
   else if (inputNode == nullptr)
      return "nullpt";
   else
      return "invald";
}
